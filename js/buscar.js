$(document).ready(function(){

	var miLista = $("#miLista");
	var busqueda = $("#busqueda");
	var div1=$("#resu");
	div1.hide();
	//miLista.append('<li>item </li>');

	$("#btn-buscar").on("click", function(){

		const palabra = $('#busqueda').val();
		console.log('Palabra a buscar: '+palabra);
		alert('Vamos a buscar: '+palabra);
		/*PONGA AQUI SU CÓDIGO*/
		$.ajax({
			url: "http://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=515e8fa309c1bb55ca29c5c8f1c8d4f9&query="+palabra,
			success: function(respuesta) {
				console.log(respuesta);
				//peliculas = $('#peliculas'); //DIV donde se cargará la lista de peliculas
				//div1.prepend("<h1>Resultados:<br>")
				setTimeout(function () {
					$('#loader').remove(); //Se elimina la imagen de "cargando" (los engranes)
	
					//Para cada elemento en la lista de resultados (para cada pelicula)
					$.each(respuesta.results, function(index, elemento) {
						//La función crearMovieCard regresa el HTML con la estructura de la pelicula
						//cardHTML = crearMovieCard(elemento); 
						miLista.append("<li>"+elemento.title+"</li>");
					});
					div1.show();
	
				}, 3000); //Tarda 3 segundos en ejecutar la función de callback
						  //Sino no se vería la imagen de los engranes, da al usuario la sensación de que se está obteniendo algo.
	
			},
			error: function() {
				console.log("No se ha podido obtener la información");
				$('#loader').remove();
				$('#miLista').html('No se ha podido obtener la información');
			},
			beforeSend: function() { 
				//ANTES de hacer la petición se muestra la imagen de cargando.
				console.log('CARGANDO');
				$('#miLista').html('<img class="mx-auto d-block" id="loader" src="images/loading.gif" />');
				div1.hide();
			},
		});	

	});

});


