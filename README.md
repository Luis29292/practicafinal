# Práctica Final
## Lenguaje de programación JavaScript
## Práctica Final: JQuery, Ajax, DOM, JSON
## Hecho por: Luis Antonio Domínguez Ávila
### Descripción del proyecto
Para este proyecto, se solicitó completar los archivos buscar.js y buscar.html, para implementar la API de búsqueda de "TheMovieDB". 
Para lograr esto, primero conseguí una API key para mi usuario y la implementé como una llamada AJAX. Después de esto, se configuró el código para que agregará el título de cada película encontrada en la búsqueda, como parte de una lista. Asímismo, se integró una pantalla de carga y un h1 que permite que el usuario sepa que esa es la lista de resultados.
